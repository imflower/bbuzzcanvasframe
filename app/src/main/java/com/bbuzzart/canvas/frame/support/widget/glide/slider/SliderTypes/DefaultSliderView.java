package com.bbuzzart.canvas.frame.support.widget.glide.slider.SliderTypes;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

import com.bbuzzart.canvas.frame.R;
import com.bbuzzart.canvas.frame.support.widget.flaviofaria.kenburnsview.KenBurnsView;
import com.bbuzzart.canvas.frame.support.widget.flaviofaria.kenburnsview.RandomTransitionGenerator;


/**
 * a simple slider view, which just show an image. If you want to make your own slider view,
 * <p>
 * just extend BaseSliderView, and implement getView() method.
 */
public class DefaultSliderView extends BaseSliderView {

	public FrameLayout background;
	public AppCompatImageView glide_slider_image;
	public KenBurnsView kenBurnsView;

	public DefaultSliderView(Context context, String fitType) {
		super(context);
		this.fitType = fitType;
	}

	public void setBackground(int color) {
		background.setBackgroundColor(color);
	}

	public String fitType;


	@Override
	public View getView() {
		View v = LayoutInflater.from(getContext()).inflate(R.layout.render_type_default, null);
		background = v.findViewById(R.id.glide_slider_background);
		kenBurnsView = v.findViewById(R.id.kenBurnsView);
		glide_slider_image = v.findViewById(R.id.glide_slider_image);


		AccelerateDecelerateInterpolator ACCELERATE_DECELERATE = new AccelerateDecelerateInterpolator();
		RandomTransitionGenerator generator = new RandomTransitionGenerator(30000, ACCELERATE_DECELERATE);
		kenBurnsView.setTransitionGenerator(generator);

		// bindEventAndShow(v, glide_slider_image);

		switch (fitType) {
			case "cover":
			case "contain":
				glide_slider_image.setVisibility(View.VISIBLE);
				kenBurnsView.setVisibility(View.GONE);

				bindEventAndShow(v, glide_slider_image);
				break;
			case "random":
				glide_slider_image.setVisibility(View.GONE);
				kenBurnsView.setVisibility(View.VISIBLE);

				bindEventAndShow(v, kenBurnsView);
				break;
			case "scroll":
				glide_slider_image.setVisibility(View.GONE);
				kenBurnsView.setVisibility(View.VISIBLE);

				bindEventAndShow(v, kenBurnsView);
				break;
		}
		return v;
	}
}
