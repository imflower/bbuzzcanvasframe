package com.bbuzzart.canvas.frame;

/**
 * Created by imflower on 13/02/2018.
 */

public interface CanvasConstant {

	static final String REMOTE_API_SERVER_URI = "https://dev2-api.bbuzzart.com/v4/";
	static final String REMOTE_IMAGE_BASE_URI = "https://djo93u9c0domr.cloudfront.net";


	static final String ACTION_NETWORK_SERVICE_START = "com.buzzart.canvas.network.start";
	static final String ACTION_NETWORK_SERVICE_FORCE_HOTSPOT = "com.buzzart.canvas.network.force.hotspot";

	static final String ACTION_CANVAS_STATE = "com.buzzart.canvas.action.CANVAS_STATE";

	static final String CANVAS_COMMAND_NETWORK_CHANGED = "CANVAS_COMMAND_NETWORK_CHANGED";
	static final String CANVAS_COMMAND_NETWORK_HOTSPOT = "CANVAS_COMMAND_NETWORK_HOTSPOT";
}
