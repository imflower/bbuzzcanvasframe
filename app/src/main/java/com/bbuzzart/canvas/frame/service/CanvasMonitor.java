package com.bbuzzart.canvas.frame.service;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.bbuzzart.canvas.frame.ui.MainActivity;

import java.util.List;

public class CanvasMonitor {

	private static final String TAG = CanvasMonitor.class.getSimpleName();

	private static CanvasMonitor instance;
	private AlarmManager am;
	private Intent intent;
	private PendingIntent sender;
	private long interval = 5000;

	private CanvasMonitor() {
	}

	public static synchronized CanvasMonitor getInstance() {
		if (instance == null) {
			instance = new CanvasMonitor();
		}
		return instance;
	}

	public static class MonitorBR extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			/*if (!isRunningService(context, CanvasMonitorService.class)) {
				context.startService(new Intent(context, CanvasMonitorService.class));
			}*/

			if (!isRunningApplication(context)) {
				context.startActivity(new Intent(context, MainActivity.class));
			}
		}
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public void startMonitoring(Context context) {
		am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		intent = new Intent(context, MonitorBR.class);
		sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		am.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), interval, sender);

		Log.w(TAG, "startMonitoring");
	}

	public void stopMonitoring(Context context) {
		am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		intent = new Intent(context, MonitorBR.class);
		sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		am.cancel(sender);
		am = null;
		sender = null;

		Log.w(TAG, "stopMonitoring");
	}

	public boolean isMonitoring() {
		return CanvasMonitorService.mThread != null && CanvasMonitorService.mThread.isAlive();
	}

	private static boolean isRunningService(Context context, Class<?> cls) {
		boolean isRunning = false;

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> info = activityManager.getRunningServices(Integer.MAX_VALUE);

		if (info != null) {
			for (ActivityManager.RunningServiceInfo serviceInfo : info) {
				ComponentName compName = serviceInfo.service;
				String className = compName.getClassName();

				if (className.equals(cls.getName())) {
					Log.w(TAG, "CLASS_NAME=" + className + ", isRunning = TRUE");
					isRunning = true;
					break;
				}
			}
		}
		return isRunning;
	}


	private static boolean isRunningApplication(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> info = activityManager.getRunningAppProcesses();
		for (int i = 0; i < info.size(); i++) {
			if (info.get(i).processName.equals(context.getPackageName())) {
				Log.w(TAG, "CLASS_NAME=" + context.getPackageName() + ", isRunning = TRUE");
				return true;
			}
		}

		return false;
	}


}
