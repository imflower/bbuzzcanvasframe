package com.bbuzzart.canvas.frame.service;

public enum NetworkState {
	UNKNOWN, WIFI, HOTSPOT;
}
