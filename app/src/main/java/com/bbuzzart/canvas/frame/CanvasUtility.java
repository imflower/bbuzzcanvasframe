package com.bbuzzart.canvas.frame;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Random;
import java.util.UUID;

public class CanvasUtility {

	private static final String PREF_NAME = "Canvas_Pref";

	public synchronized static String getDeviceId(Context context) {
		final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
		String uniqueID = "";

		SharedPreferences sharedPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
		if (TextUtils.isEmpty(uniqueID)) {
			uniqueID = UUID.randomUUID().toString();
			SharedPreferences.Editor editor = sharedPrefs.edit();
			editor.putString(PREF_UNIQUE_ID, uniqueID);
			editor.apply();
		}

		return uniqueID;
	}

	public synchronized static String getHotspotName(Context context) {
		final String PREF_HOTSPOT_NAME = "PREF_HOTSPOT_NAME";
		String hotspotName = "";

		SharedPreferences sharedPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		hotspotName = sharedPrefs.getString(PREF_HOTSPOT_NAME, null);
		if (TextUtils.isEmpty(hotspotName)) {
			Random random = new Random();
			String tag = Long.toString(Math.abs(random.nextLong()), 10);
			hotspotName = "BBuzzCanvas-" + tag.substring(0, 6).toUpperCase();

			SharedPreferences.Editor editor = sharedPrefs.edit();
			editor.putString(PREF_HOTSPOT_NAME, hotspotName);
			editor.apply();
		}

		return hotspotName;
	}

	public synchronized static String getAuth(Context context, String auth) {
		final String PREF_USER_AUTH = "PREF_USER_AUTH";

		SharedPreferences sharedPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

		if (!TextUtils.isEmpty(auth)) {
			SharedPreferences.Editor editor = sharedPrefs.edit();
			editor.putString(PREF_USER_AUTH, auth);
			editor.apply();
		} else {
			auth = sharedPrefs.getString(PREF_USER_AUTH, null);
		}

		return auth;
	}

	public synchronized static void clearPreference(Context context) {
		SharedPreferences sharedPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.clear();
		editor.apply();
	}


	public static boolean isWifiConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		Network[] networks = cm != null ? cm.getAllNetworks() : new Network[0];
		NetworkInfo networkInfo;
		for (Network mNetwork : networks) {
			networkInfo = cm.getNetworkInfo(mNetwork);
			if (networkInfo == null)
				continue;

			if (!TextUtils.isEmpty(networkInfo.getExtraInfo()) &&
					networkInfo.getType() == ConnectivityManager.TYPE_WIFI &&
					networkInfo.getState() == NetworkInfo.State.CONNECTED) {
				return true;
			}
		}

		return false;
	}

	public static InetAddress getNetworkAddress() throws SocketException {
		for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
			NetworkInterface network = en.nextElement();
			for (Enumeration<InetAddress> addresses = network.getInetAddresses(); addresses.hasMoreElements(); ) {
				InetAddress inetAddress = addresses.nextElement();
				if ((inetAddress instanceof Inet4Address) && !inetAddress.isLoopbackAddress()) {
					return inetAddress;
				}
			}
		}

		return null;
	}

	public static String getApIpAddress(Context context) {
		WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
		byte[] ipAddress = convert2Bytes(dhcpInfo.serverAddress);
		try {
			return InetAddress.getByAddress(ipAddress).getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static byte[] convert2Bytes(int hostAddress) {
		byte[] addressBytes = {
				(byte) (0xff & hostAddress),
				(byte) (0xff & (hostAddress >> 8)),
				(byte) (0xff & (hostAddress >> 16)),
				(byte) (0xff & (hostAddress >> 24))
		};
		return addressBytes;
	}


}
