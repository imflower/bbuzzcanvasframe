package com.bbuzzart.canvas.frame.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bbuzzart.canvas.frame.CanvasApplication;
import com.bbuzzart.canvas.frame.CanvasUtility;
import com.bbuzzart.canvas.frame.R;
import com.bbuzzart.canvas.frame.support.widget.flaviofaria.kenburnsview.KenBurnsView;
import com.bbuzzart.canvas.frame.support.widget.glide.slider.Indicators.PagerIndicator;
import com.bbuzzart.canvas.frame.support.widget.glide.slider.SliderLayout;
import com.bbuzzart.canvas.frame.support.widget.glide.slider.SliderTypes.BaseSliderView;
import com.bbuzzart.canvas.frame.support.widget.glide.slider.SliderTypes.DefaultSliderView;
import com.bbuzzart.canvas.frame.support.widget.glide.slider.SliderTypes.TextSliderView;
import com.bbuzzart.canvas.frame.support.widget.glide.slider.Tricks.ViewPagerEx;
import com.bbuzzart.canvas.network.model.Canvas;
import com.bbuzzart.canvas.network.model.CanvasViewCount;
import com.bbuzzart.canvas.network.model.Pick;
import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ShowFragment extends Fragment implements BaseSliderView.OnSliderClickListener,
		ViewPagerEx.OnPageChangeListener {

	public static final String TAG = ShowFragment.class.getSimpleName();

	private CanvasApplication application;

	private Unbinder unbinder;


	@BindView(R.id.slider)
	SliderLayout slider;

	@BindView(R.id.picture)
	FrameLayout picture;
	@BindView(R.id.pictureRandomView)
	KenBurnsView pictureRandomView;
	@BindView(R.id.pictureImageView)
	ImageView pictureImageView;


	private List<Pick> picks;

	public ShowFragment() {

	}

	public static ShowFragment newInstance(List<Pick> picks) {
		ShowFragment fragment = new ShowFragment();
		Bundle args = new Bundle();
		args.putParcelable("picks", Parcels.wrap(picks));
		fragment.setArguments(args);

		return fragment;
	}


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_show, container, false);
		unbinder = ButterKnife.bind(this, view);

		return view;
	}


	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		application = (CanvasApplication) getActivity().getApplicationContext();
		compositeDisposable = new CompositeDisposable();

		if (getArguments() != null) {
			this.picks = Parcels.unwrap(getArguments().getParcelable("picks"));
		}

		Log.w(TAG, "picks SIZE = " + (picks != null ? picks.size() : 0));
		/*for (Pick pick : picks) {
			Log.w(TAG, "pick==" + pick.toString());
		}*/

		String cdnDomain = application.getBuzzArtServerConfig().getData().getCdn().getDomain();
		Canvas.CanvasSetting setting = application.getCanvas().getCanvasSetting();

		if (setting.getData().getType().equals("curation")) {
			picture.setVisibility(View.GONE);
			slider.setVisibility(View.VISIBLE);


			Log.w(TAG, setting.getFit().getType());
			RequestOptions requestOptions = new RequestOptions();

			switch (setting.getFit().getType()) {
				case "cover":
					requestOptions.centerCrop();
					break;
				case "random":
					requestOptions.centerCrop();
					break;
				case "scroll":
					requestOptions.centerCrop();
					break;
				case "contain":
					requestOptions.centerInside();
					break;
				default:
					requestOptions.centerInside();
					break;
			}


			for (Pick pick : picks) {
				if (setting.getCaption().getUse()) {
					TextSliderView sliderView = new TextSliderView(application, setting.getFit().getType());

					switch (setting.getStyle().getBackgroundColor()) {
						case "black":
							sliderView.setBackgroundColor(Color.BLACK);
							break;
						case "white":
							sliderView.setBackgroundColor(Color.WHITE);
							break;
						case "gray":
							sliderView.setBackgroundColor(Color.GRAY);
							break;
					}

					sliderView
							.image(cdnDomain + pick.getWorkImgmUrl())
							.description(pick.getWorkTitle())
							.fitType(setting.getFit().getType())
							.artistName("By " + pick.getArtistName())
							.setRequestOption(requestOptions)
							.setProgressBarVisible(true)
							.setOnSliderClickListener(this);

					// /*add your extra information*/
					// sliderView.bundle(new Bundle());
					// sliderView.getBundle().putString("extra", pick.getWorkTitle());
					slider.addSlider(sliderView);

					// DescriptionAnimation descriptionAnimation = new DescriptionAnimation();
					// descriptionAnimation.duration = 10 * 1000;
					// slider.setCustomAnimation(descriptionAnimation);
				} else {
					DefaultSliderView sliderView = new DefaultSliderView(application, setting.getFit().getType());

					switch (setting.getStyle().getBackgroundColor()) {
						case "black":
							sliderView.setBackgroundColor(Color.BLACK);
							break;
						case "white":
							sliderView.setBackgroundColor(Color.WHITE);
							break;
						case "gray":
							sliderView.setBackgroundColor(Color.GRAY);
							break;
					}


					sliderView
							.image(cdnDomain + pick.getWorkImgmUrl())
							.fitType(setting.getFit().getType())
							.setRequestOption(requestOptions)
							.setProgressBarVisible(true)
							.setOnSliderClickListener(this);

					slider.addSlider(sliderView);
				}
			}

			// INDICATOR
			slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
			slider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);


			// TRANSITION
			if (setting.getTransition().getType().equals("slide"))
				slider.setPresetTransformer(SliderLayout.Transformer.Default);
			else
				slider.setPresetTransformer(SliderLayout.Transformer.Fade);

			slider.setSliderTransformDuration(setting.getTransition().getDuration() * 1000, null);


			// DURATION
			int duration = setting.getInterval().getHour() * 3600 * 1000 + setting.getInterval().getMinute() * 60 * 1000;
			slider.setDuration(duration); // TODO TEST 15second,  - 45000
			slider.startAutoCycle();

			slider.addOnPageChangeListener(this);

			doPostPickViewCount(0);

		} else {
			picture.setVisibility(View.VISIBLE);
			slider.setVisibility(View.GONE);


			switch (setting.getStyle().getBackgroundColor()) {
				case "black":
					picture.setBackgroundColor(Color.BLACK);
					break;
				case "white":
					picture.setBackgroundColor(Color.WHITE);
					break;
				case "gray":
					picture.setBackgroundColor(Color.GRAY);
					break;
			}

			RequestOptions requestOptions = new RequestOptions();
			switch (setting.getFit().getType()) {
				case "cover":
					pictureRandomView.setVisibility(View.GONE);
					pictureImageView.setVisibility(View.VISIBLE);

					requestOptions.centerCrop();
					Glide.with(application)
							.load(cdnDomain + picks.get(0).getWorkImgmUrl())
							.apply(requestOptions)
							.into(pictureImageView);
					break;
				case "random":
					pictureRandomView.setVisibility(View.VISIBLE);
					pictureImageView.setVisibility(View.GONE);

					requestOptions.centerCrop();
					Glide.with(application)
							.load(cdnDomain + picks.get(0).getWorkImgmUrl())
							.apply(requestOptions)
							.into(pictureRandomView);
					break;
				case "scroll":
					break;
				case "contain":
					pictureRandomView.setVisibility(View.GONE);
					pictureImageView.setVisibility(View.VISIBLE);

					requestOptions.centerInside();
					Glide.with(application)
							.load(cdnDomain + picks.get(0).getWorkImgmUrl())
							.apply(requestOptions)
							.into(pictureImageView);
					break;
				default:
					pictureRandomView.setVisibility(View.GONE);
					pictureImageView.setVisibility(View.VISIBLE);

					requestOptions.centerInside();
					Glide.with(application)
							.load(cdnDomain + picks.get(0).getWorkImgmUrl())
							.apply(requestOptions)
							.into(pictureImageView);
					break;
			}

			doPostPickViewCount(0);
		}

		Log.w(TAG, "onViewCreated");
	}

	@Override
	public void onDestroyView() {
		if (slider != null) {
			slider.stopAutoCycle();
			slider = null;
		}

		unbinder.unbind();
		super.onDestroyView();
	}


	@Override
	public void onSliderClick(BaseSliderView baseSliderView) {

	}

	@Override
	public void onPageScrolled(int i, float v, int i1) {

	}

	@Override
	public void onPageSelected(int i) {
		Log.w(TAG, "onPageSelected, TITLE=" + picks.get(i).getWorkTitle());

		doPostPickViewCount(i);
	}

	@Override
	public void onPageScrollStateChanged(int i) {

	}


	private CompositeDisposable compositeDisposable;

	private void doPostPickViewCount(int index) {
		BuzzArtService service = application.getBuzzArtService();

		int curationId = picks.get(index).getCurationId();
		int pickId = picks.get(index).getPickId();
		CanvasViewCount form = new CanvasViewCount(application.getCanvas().getCanvasId());
		String auth = CanvasUtility.getAuth(application, null);

		Disposable disposable = service.doPostPickViewCount(curationId, pickId, auth, form)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						result -> {
							Log.w(TAG, "doPostPickViewCount Result, " + result.toString());
							if (result.getStatus()) {
								// SUCCESS
								Log.w(TAG, "Success");
							} else {
								Log.w(TAG, result.toString());
							}
						},
						throwable -> Log.e(TAG, "doPostPickViewCount Error", throwable)
				);

		compositeDisposable.add(disposable);
	}

}
