package com.bbuzzart.canvas.frame.ui;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bbuzzart.canvas.frame.CanvasApplication;
import com.bbuzzart.canvas.frame.CanvasConstant;
import com.bbuzzart.canvas.frame.CanvasUtility;
import com.bbuzzart.canvas.frame.R;
import com.bbuzzart.canvas.frame.service.CanvasMonitor;
import com.bbuzzart.canvas.frame.service.NetworkService;
import com.bbuzzart.canvas.frame.ui.fragment.KenBurnsFragment;
import com.bbuzzart.canvas.frame.ui.fragment.ShowFragment;
import com.bbuzzart.canvas.network.model.Canvas;
import com.bbuzzart.canvas.network.model.CanvasRegistModel;
import com.bbuzzart.canvas.network.service.BuzzArtService;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.socket.client.IO;
import io.socket.client.Socket;

public class MainActivity extends AppCompatActivity implements CanvasConstant {

	private static final String TAG = MainActivity.class.getSimpleName();
	private static final long FINISH_INTERVAL_TIME = 1000L;

	private CanvasMonitor canvasMonitor;

	CanvasApplication application;
	LocalBroadcastReceiver localBroadcastReceiver;
	FragmentManager fragmentManager;
	Socket socket;
	@BindView(R.id.logoView)
	FrameLayout logoView;
	@BindView(R.id.defaultShowView)
	FrameLayout defaultShowView;


	@BindView(R.id.sliderLayout)
	RelativeLayout sliderLayout;


	KenBurnsFragment kenBurnsFragment;
	ShowFragment showFragment;
	private Unbinder unbinder;
	private long backPressedTime;
	private CompositeDisposable compositeDisposable;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		unbinder = ButterKnife.bind(this);
		fragmentManager = getSupportFragmentManager();
		application = (CanvasApplication) getApplicationContext();
		compositeDisposable = new CompositeDisposable();

		/*canvasMonitor = CanvasMonitor.getInstance();
		if (!canvasMonitor.isMonitoring()) {
			canvasMonitor.startMonitoring(getApplicationContext());
		}*/

		defaultShowView.setVisibility(View.GONE);
		sliderLayout.setVisibility(View.GONE);

		initBroadcastReceiver();

		// CanvasUtility.clearPreference(getApplicationContext());

		Log.e(TAG, "HOTSPOT_NAME=" + CanvasUtility.getHotspotName(getApplicationContext()));
		Log.e(TAG, "DEVICE_ID=" + CanvasUtility.getDeviceId(getApplicationContext()));

		Intent intent = new Intent(getApplicationContext(), NetworkService.class);
		intent.setAction(ACTION_NETWORK_SERVICE_START);
		startService(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		long tempTime = System.currentTimeMillis();
		long intervalTime = tempTime - backPressedTime;
		if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime) {
			finish();
		} else {
			backPressedTime = tempTime;
			Toast.makeText(MainActivity.this, getString(R.string.app_finish), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onDestroy() {
		unbinder.unbind();
		reset();
		stopService(new Intent(getApplicationContext(), NetworkService.class));
		canvasMonitor.stopMonitoring(getApplicationContext());

		super.onDestroy();
	}


	private void changeConnectionState() {
		new Handler().postDelayed(() -> {
			boolean isConnected = CanvasUtility.isWifiConnected(getApplicationContext());

			if (isConnected) {
				initServer();
			} else {
				if (showFragment != null) {
					sliderLayout.setVisibility(View.INVISIBLE);

					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.remove(showFragment);
					fragmentTransaction.commit();

					fragmentManager.popBackStack();
					showFragment = null;
				}

				if (kenBurnsFragment != null) {
					defaultShowView.setVisibility(View.INVISIBLE);

					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.remove(kenBurnsFragment);
					fragmentTransaction.commit();

					fragmentManager.popBackStack();
					kenBurnsFragment = null;
				}

				defaultShowView.setVisibility(View.VISIBLE);

				kenBurnsFragment = new KenBurnsFragment();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.defaultShowFragment, kenBurnsFragment);
				fragmentTransaction.addToBackStack(KenBurnsFragment.TAG);
				fragmentTransaction.commit();

				new Handler().postDelayed(() -> {
					kenBurnsFragment.doChangeNetworkState(false);
				}, 500);

			}
		}, 1000);
	}

	private void initBroadcastReceiver() {
		localBroadcastReceiver = new LocalBroadcastReceiver();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ACTION_CANVAS_STATE);
		intentFilter.addAction("SHOW_CURATION");

		LocalBroadcastManager.getInstance(this).registerReceiver(localBroadcastReceiver, intentFilter);
	}

	@SuppressLint("HardwareIds")
	private void initServer() {
		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchInitialize()
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						result -> {
							Log.w(TAG, "Initialize Result");
							Log.w(TAG, result.toString());
							if (result.getStatus()) {
								// SUCCESS
								application.setBuzzArtServerConfig(result);

								String auth = CanvasUtility.getAuth(application, null);
								if (TextUtils.isEmpty(auth)) {
									Intent intent = new Intent(getApplicationContext(), NetworkService.class);
									intent.setAction(ACTION_NETWORK_SERVICE_FORCE_HOTSPOT);
									startService(intent);
								} else {
									doCanvasInit();
								}
							} else {
								Log.w(TAG, result.toString());
							}
						},
						throwable -> Log.e(TAG, throwable.toString())
				);

		compositeDisposable.add(disposable);
	}

	private void doCanvasInit() {
		BuzzArtService service = application.getBuzzArtService();

		WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		String mac = wifiManager.getConnectionInfo().getMacAddress();
		String ssid = wifiManager.getConnectionInfo().getSSID().replaceAll("\"", "");

		String auth = CanvasUtility.getAuth(application, null);
		Log.w(TAG, "auth=" + auth + ", mac=" + mac);

		Disposable disposable = service.doGetCanvasInit(auth, mac)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						result -> {
							if (result.getStatus()) {
								// SUCCESS
								Log.w(TAG, "result=" + result.getData().toString());
								if (result.getData().getCanvas() == null) {
									doCanvasRegist(mac, ssid);
								} else {
									application.setCanvas(result.getData().getCanvas());

									Canvas.CanvasSetting setting = result.getData().getCanvas().getCanvasSetting();

									if (setting.getData().getType().equals("curation")) {
										fetchCuratorPicks(setting.getData().getCurationId());
									} else if (setting.getData().getType().equals("pick")) {
										fetchCuratorPickSingle(setting.getData().getCurationId(), setting.getData().getPickId());
									}

									try {
										// WEB_SOCKET
										IO.Options opts = new IO.Options();
										opts.forceNew = true;
										opts.reconnection = true;

										socket = IO.socket("https://dev2-canvas.bbuzzart.com/", opts);
										// socket = IO.socket("https://newsfeed.bbuzzart.com/");
										socket
												.on(Socket.EVENT_CONNECT, args -> {
													int canvasId = application.getCanvas().getCanvasId();
													// String auth = CanvasUtility.getAuth(application, null);

													JSONObject data = new JSONObject();
													try {
														data.put("canvasId", canvasId);
														data.put("auth", auth);
														socket.emit("init", data);
													} catch (JSONException e) {
														Log.e(TAG, "JSON Error", e);
													}

													// socket.emit("init", "{\"canvasId\" : " + canvasId + ", \"auth\" : \"" + auth + "\"}");
													Log.w(TAG, "CONNECT");
												})
												.on("change", args -> {
													Log.w(TAG, "CHANGE");

													doCanvasInit();
												})
												.on(Socket.EVENT_DISCONNECT, args -> Log.w(TAG, "DISCONNECT"));
										socket.connect();
									} catch (Exception e) {
										Log.e(TAG, "WS Error", e);
										if (socket != null) {
											socket.disconnect();
											socket.close();
											socket = null;
										}
									}

								}
							} else {
								Log.e(TAG, result.toString());
							}
						},
						throwable -> Log.e(TAG, "doCanvasInit ERR", throwable)
				);

		compositeDisposable.add(disposable);
	}

	private void doCanvasRegist(String mac, String ssid) {
		BuzzArtService service = application.getBuzzArtService();

		String name = CanvasUtility.getHotspotName(application);
		CanvasRegistModel model = new CanvasRegistModel(name, name, mac, ssid);

		String auth = CanvasUtility.getAuth(application, null);
		Log.w(TAG, "auth=" + auth + ", model=" + model.toString());

		Disposable disposable = service.doPostCanvasRegist(auth, model)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						result -> {
							if (result.getStatus()) {
								Log.w(TAG, "RESULT-" + result.toString());
								application.setCanvas(result.getData().getCanvas());

								Canvas.CanvasSetting setting = result.getData().getCanvas().getCanvasSetting();

								if (setting.getData().getType().equals("curation")) {
									fetchCuratorPicks(setting.getData().getCurationId());
								} else if (setting.getData().getType().equals("pick")) {
									fetchCuratorPickSingle(setting.getData().getCurationId(), setting.getData().getPickId());
								}

							} else {
								Log.e(TAG, "RESULT-" + result.toString());
							}
						},
						throwable -> Log.e(TAG, "doCanvasRegist ERR", throwable)
				);

		compositeDisposable.add(disposable);
	}

	private void reset() {
		if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
			compositeDisposable.dispose();
		}

		compositeDisposable = null;
	}

	private void fetchCuratorPicks(int curatorId) {
		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchCuratorPicks(curatorId, null, true, 1, 10)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						picks -> {
							if (picks.getStatus()) {
								Log.w(TAG, picks.getData().getPicks().toString());

								if (kenBurnsFragment != null) {
									defaultShowView.setVisibility(View.INVISIBLE);

									FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
									fragmentTransaction.remove(kenBurnsFragment);
									fragmentTransaction.commit();

									fragmentManager.popBackStack();
									kenBurnsFragment = null;
								}

								if (showFragment != null) {
									sliderLayout.setVisibility(View.INVISIBLE);

									FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
									fragmentTransaction.remove(showFragment);
									fragmentTransaction.commit();

									fragmentManager.popBackStack();
									showFragment = null;
								}

								sliderLayout.setVisibility(View.VISIBLE);
								showFragment = ShowFragment.newInstance(picks.getData().getPicks());

								new Handler().postDelayed(() -> {
									FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
									fragmentTransaction.replace(R.id.showFragment, showFragment);
									fragmentTransaction.addToBackStack(ShowFragment.TAG);
									fragmentTransaction.commit();
								}, 200);

							} else {
								// TODO fail
								Log.e("Model", picks.toString());
							}
						},
						throwable -> {
							Log.e("Model", "fetchCuratorPicks Error", throwable);
						}
				);
		compositeDisposable.add(disposable);
	}

	private void fetchCuratorPickSingle(int curatorId, int pickId) {
		String auth = CanvasUtility.getAuth(application, null);

		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchCuratorPickSingle(curatorId, pickId, auth)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						picks -> {
							if (picks.getStatus()) {
								Log.w(TAG, picks.getData().getPagination().toString());

								if (kenBurnsFragment != null) {
									defaultShowView.setVisibility(View.INVISIBLE);

									FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
									fragmentTransaction.remove(kenBurnsFragment);
									fragmentTransaction.commit();

									fragmentManager.popBackStack();
									kenBurnsFragment = null;
								}

								if (showFragment != null) {
									sliderLayout.setVisibility(View.INVISIBLE);

									FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
									fragmentTransaction.remove(showFragment);
									fragmentTransaction.commit();

									fragmentManager.popBackStack();
									showFragment = null;
								}

								sliderLayout.setVisibility(View.VISIBLE);
								showFragment = ShowFragment.newInstance(picks.getData().getPicks());

								new Handler().postDelayed(() -> {
									FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
									fragmentTransaction.replace(R.id.showFragment, showFragment);
									fragmentTransaction.addToBackStack(ShowFragment.TAG);
									fragmentTransaction.commit();
								}, 200);
							} else {
								// TODO fail
								Log.e("Model", picks.toString());
							}
						},
						throwable -> {
							Log.e("Model", "fetchCuratorPicks Error", throwable);
						}
				);
		compositeDisposable.add(disposable);
	}

	@Override
	protected void onStop() {
		if (socket != null) {
			socket.disconnect();
			socket.close();
			socket = null;
		}

		super.onStop();
	}

	private class LocalBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.w(TAG, "RECEIVED ACTION=" + action);

			if (action != null) {
				switch (action) {
					case ACTION_CANVAS_STATE: {
						try {
							new Handler().postDelayed(() -> {
								String command = intent.getStringExtra("command");
								if (command.equals(CANVAS_COMMAND_NETWORK_CHANGED))
									changeConnectionState();
								else if (command.equals(CANVAS_COMMAND_NETWORK_HOTSPOT)) {
									changeConnectionState();
								}
							}, 500);
						} catch (Exception e) {
							Log.e(TAG, "XXXX", e);
						}
						break;
					}
				}
			}
		}
	}



}
