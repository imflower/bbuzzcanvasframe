package com.bbuzzart.canvas.frame.support;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.lang.reflect.Method;

public class HotspotControl {

	private static final String TAG = "Hotspot";

	private static Method getWifiApState;
	private static Method isWifiApEnabled;
	private static Method setWifiApEnabled;
	private static Method getWifiApConfiguration;
	private static Method setWifiApConfiguration;

	static {
		// lookup methods and fields not defined publicly in the SDK.
		Class<?> cls = WifiManager.class;
		for (Method method : cls.getDeclaredMethods()) {
			String methodName = method.getName();
			switch (methodName) {
				case "getWifiApState":
					getWifiApState = method;
					break;
				case "isWifiApEnabled":
					isWifiApEnabled = method;
					break;
				case "setWifiApEnabled":
					setWifiApEnabled = method;
					break;
				case "getWifiApConfiguration":
					getWifiApConfiguration = method;
					break;
				case "setWifiApConfiguration":
					setWifiApConfiguration = method;
					break;
			}
		}
	}

	public static boolean isApSupported() {
		return (getWifiApState != null && isWifiApEnabled != null && setWifiApEnabled != null &&
				getWifiApConfiguration != null && setWifiApConfiguration != null);
	}

	private WifiManager mgr;

	private HotspotControl(WifiManager mgr) {
		this.mgr = mgr;
	}

	public static HotspotControl getApControl(WifiManager mgr) {
		if (!isApSupported())
			return null;
		return new HotspotControl(mgr);
	}

	public boolean isWifiApEnabled() {
		try {
			return (Boolean) isWifiApEnabled.invoke(mgr);
		} catch (Exception e) {
			Log.e(TAG, "isWifiApEnabled Error", e);
			return false;
		}
	}

	public int getWifiApState() {
		try {
			return (Integer) getWifiApState.invoke(mgr);
		} catch (Exception e) {
			Log.e(TAG, "getWifiApState Error", e);
			return -1;
		}
	}

	public WifiConfiguration getWifiApConfiguration() {
		try {
			return (WifiConfiguration) getWifiApConfiguration.invoke(mgr);
		} catch (Exception e) {
			Log.e(TAG, "getWifiApConfiguration Error", e);
			return null;
		}
	}

	public boolean setHotspotName(WifiConfiguration config, String name) {
		try {
			config = new WifiConfiguration();
			config.allowedAuthAlgorithms.clear();
			config.allowedGroupCiphers.clear();
			config.allowedKeyManagement.clear();
			config.allowedPairwiseCiphers.clear();
			config.allowedProtocols.clear();

			config.SSID = name;
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

			return (Boolean) setWifiApConfiguration.invoke(mgr, config);
		} catch (Exception e) {
			Log.e(TAG, "setHotspotName Error", e);
			return false;
		}
	}

	public boolean setWifiApEnabled(WifiConfiguration config, boolean enabled) {
		try {
			return (Boolean) setWifiApEnabled.invoke(mgr, config, enabled);
		} catch (Exception e) {
			Log.e(TAG, "setWifiApEnabled Error", e);
			return false;
		}
	}

}
