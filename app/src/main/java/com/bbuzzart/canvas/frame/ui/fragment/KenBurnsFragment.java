package com.bbuzzart.canvas.frame.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bbuzzart.canvas.frame.CanvasUtility;
import com.bbuzzart.canvas.frame.R;
import com.bbuzzart.canvas.frame.support.widget.kenburns.KenBurnsImageView;
import com.bbuzzart.canvas.frame.support.widget.kenburns.LoopViewPager;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class KenBurnsFragment extends Fragment {

	public static final String TAG = KenBurnsFragment.class.getSimpleName();

	private Unbinder unbinder;

	@BindView(R.id.networkNotFoundView)
	FrameLayout networkNotFoundView;
	@BindView(R.id.kenBurnsImageView)
	KenBurnsImageView kenBurnsImageView;
	@BindView(R.id.kenBurnsImageViewPagerFrameLayout)
	FrameLayout kenBurnsImageViewPagerFrameLayout;

	@BindView(R.id.txtNetworkSSID)
	TextView txtNetworkSSID;
	@BindView(R.id.txtNetworkMessage)
	TextView txtNetworkMessage;


	public KenBurnsFragment() {

	}


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_kenburns_view, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		unbinder = ButterKnife.bind(this, view);

		txtNetworkSSID.setText(CanvasUtility.getHotspotName(Objects.requireNonNull(getActivity())));

		initializeKenBurnsView();

		new Handler().postDelayed(() -> {
			// kenBurnsImageView.stopKenBurnsAnimation();
			kenBurnsImageView.startKenBurnsAnimation();
			// doChangeNetworkState(CanvasUtility.isWifiConnected(getActivity()));
		}, 2000);
	}

	@Override
	public void onDestroyView() {
		unbinder.unbind();
		super.onDestroyView();
	}

	public void doChangeNetworkState(boolean isConnected) {
		if (isConnected) {
			networkNotFoundView.setVisibility(View.GONE);
		} else {
			networkNotFoundView.setVisibility(View.VISIBLE);
		}
	}

	private void initializeKenBurnsView() {
		kenBurnsImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		kenBurnsImageView.setSwapMs(15000);
		kenBurnsImageView.setFadeInOutMs(3000);
		kenBurnsImageView.setMaxScaleFactor(1.5f);
		kenBurnsImageView.setMinScaleFactor(1.0f);

		List<Integer> resourceIDs = Arrays.asList(SampleImages.IMAGES_RESOURCE);
		Collections.shuffle(resourceIDs);
		kenBurnsImageView.loadResourceIDs(resourceIDs);

		LoopViewPager.LoopViewPagerListener listener = new LoopViewPager.LoopViewPagerListener() {
			@Override
			public View OnInstantiateItem(int page) {
				TextView counterText = new TextView(getActivity());
				counterText.setText(String.valueOf(page));
				return counterText;
			}

			@Override
			public void onPageScroll(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
				kenBurnsImageView.forceSelected(position);
			}

			@Override
			public void onPageScrollChanged(int page) {
			}
		};

		// LoopView
		LoopViewPager loopViewPager = new LoopViewPager(getActivity(), resourceIDs.size(), listener);
		kenBurnsImageViewPagerFrameLayout.addView(loopViewPager);
		kenBurnsImageView.setPager(loopViewPager);
	}

	public static class SampleImages {
		static final Integer[] IMAGES_RESOURCE = new Integer[]{
				R.drawable.img_4294_20151015040538,
				R.drawable.img_38515_20170720123823,
				R.drawable.img_38552_20170721065004,
				R.drawable.img_38738_20170725010800,
				R.drawable.img_42792_20170821201317,
				R.drawable.img_44135_20170829193613,
				R.drawable.img_44594_20170903183033,
				R.drawable.img_44686_20170904110407,
				R.drawable.img_44695_20170904135029,
				R.drawable.img_45865_20170917114359,
				R.drawable.img_47227_20171011102651,
				R.drawable.img_47315_20171011183431,
				R.drawable.img_48457_20171018143610,
				R.drawable.img_48509_20171018190102,
				R.drawable.img_49061_20171023105923,
				R.drawable.img_49669_20171030234244,
				R.drawable.img_50975_20171119062441,
				R.drawable.img_52492_20171219072008,
				R.drawable.img_65954_20180125195506,
				R.drawable.img_66085_20180129082911,
				R.drawable.img_66611_20180206212419,
				R.drawable.img_67072_20180212134108,
				R.drawable.img_67122_20180212151840,
				R.drawable.img_38473_20170719101602,
		};
	}

}
