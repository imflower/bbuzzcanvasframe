package com.bbuzzart.canvas.frame.service;

/**
 * Created by imflower on 26/03/2018.
 */

public interface NetworkConstant {

	public static final int MSG_COMMAND_HOTSPOT_TURN_ON = 0x0001;
	public static final int MSG_COMMAND_HOTSPOT_TURN_OFF = 0x0002;

	public static final int MSG_WIFI_TURN_ON_SUCCESS = 0x1001;
	public static final int MSG_WIFI_TURN_ON_FAIL = 0x1002;


	public static final int MSG_HOTSPOT_TURN_ON_SUCCESS = 0x6001;
	public static final int MSG_HOTSPOT_TURN_ON_FAIL = 0x6002;

}
