package com.bbuzzart.canvas.frame.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bbuzzart.canvas.frame.CanvasConstant;
import com.bbuzzart.canvas.frame.CanvasUtility;
import com.bbuzzart.canvas.frame.support.HotspotControl;
import com.bbuzzart.canvas.frame.support.NanoHttp;
import com.bbuzzart.canvas.frame.support.wifi.WifiConfigManager;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import lombok.Getter;

public class NetworkService extends Service implements NetworkConstant, CanvasConstant {

	public static final String TAG = NetworkService.class.getSimpleName();

	private static final int SERVER_PORT = 9999;


	private Handler hotspotHandler;
	private Handler commandHandler;

	private Thread hotspotTurnOn;
	private NetworkRunnable networkRunnable;
	private NetworkHttpServer httpServer;


	private WifiManager wifiManager;
	private ConnectivityManager cm;
	private ConnectivityManager.NetworkCallback networkCallback;
	private boolean isAvoidFlag;

	private boolean isWifiConnectState;

	private BroadcastReceiver broadcastReceiver;


	@Getter
	private NetworkState networkState;

	private void setNetworkState(NetworkState state) {
		this.networkState = state;
		Log.e(TAG, "NetworkState=" + this.networkState);
	}


	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.w(TAG, "onCreate");
		super.onCreate();

		isWifiConnectState = false;

		this.wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		this.cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		initIntentFilter();

		initHandler();
		initNetworkState();
		initNetworkRunnable();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent == null) {
			stopSelf();
		} else {
			String action = intent.getAction();
			if (TextUtils.isEmpty(action)) {
				action = "";
			} else {
				Log.w(TAG, "Main Intent Action: " + action);
			}

			switch (action) {
				case ACTION_NETWORK_SERVICE_START: {
					parseActionStart();
					break;
				}
				case ACTION_NETWORK_SERVICE_FORCE_HOTSPOT: {
					setNetworkState(NetworkState.UNKNOWN);
					stopServer();

					isPrevConnectedWifi = false;
					if (cm != null && networkCallback != null) {
						cm.unregisterNetworkCallback(networkCallback);
						cm = null;
						networkCallback = null;
					}

					new Handler().postDelayed(() -> {
						initNetworkState();

						commandHandler.sendEmptyMessage(MSG_COMMAND_HOTSPOT_TURN_ON);
					}, 500);
					break;
				}
			}
		}

		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy() {
		if (broadcastReceiver != null)
			unregisterReceiver(broadcastReceiver);

		stopServer();

		if (cm != null && networkCallback != null) {
			cm.unregisterNetworkCallback(networkCallback);
			cm = null;
			networkCallback = null;
		}

		super.onDestroy();
	}

	private void initIntentFilter() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

		broadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if (action != null) {
					switch (action) {
						case WifiManager.NETWORK_STATE_CHANGED_ACTION: {
							if (!isWifiConnectState)
								return;

							NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
							Log.w(TAG, "NetInfo=" + info.toString());

							if (info.isConnected()) {
								isWifiConnectState = false;

								stopServer();
								startServer();

								/*
								canvasWifiInfo = wifiManager.getConnectionInfo();
								canvasDhcpInfo = wifiManager.getDhcpInfo();

								Log.w(TAG, "WIFI_INFO=" + canvasWifiInfo.toString());
								Log.e(TAG, "DHCP_INFO=" + canvasDhcpInfo.toString());
								*/
								sendBroadcast();
							} else {
								// wifi connection was lost
							}
							break;
						}
					}
				}
			}
		};


		registerReceiver(broadcastReceiver, intentFilter);
	}

	private void parseActionStart() {
		if (!CanvasUtility.isWifiConnected(getApplicationContext())) {
			Log.w(TAG, "Network not connected");
			setNetworkState(NetworkState.UNKNOWN);

			commandHandler.sendEmptyMessage(MSG_COMMAND_HOTSPOT_TURN_ON);
		} else {
			setNetworkState(NetworkState.WIFI);

			startServer();
		}
	}

	private void initHandler() {
		commandHandler = new Handler(msg -> {
			switch (msg.what) {
				case MSG_COMMAND_HOTSPOT_TURN_ON: {
					Log.w(TAG, "MSG_COMMAND_HOTSPOT_TURN_ON");
					if (hotspotTurnOn == null || !hotspotTurnOn.isAlive()) {
						hotspotTurnOn = new Thread(networkRunnable.HotspotTurnOn);
						hotspotTurnOn.start();
					} else {
						Log.e(TAG, "MSG_COMMAND_HOTSPOT_TURN_ON running");
					}
					break;
				}
			}
			return false;
		});

		hotspotHandler = new Handler(msg -> {
			switch (msg.what) {
				case MSG_HOTSPOT_TURN_ON_FAIL: {
					Log.w(TAG, "MSG_HOTSPOT_TURN_ON_FAIL");

					if (hotspotTurnOn != null) {
						hotspotTurnOn.interrupt();
						hotspotTurnOn = null;
					}

					stopServer();
					break;
				}
				case MSG_HOTSPOT_TURN_ON_SUCCESS: {
					Log.w(TAG, "MSG_HOTSPOT_TURN_ON_SUCCESS");

					hotspotTurnOn = null;

					setNetworkState(NetworkState.HOTSPOT);

					stopServer();
					startServer();

					Intent intent = new Intent(ACTION_CANVAS_STATE);
					intent.putExtra("command", CANVAS_COMMAND_NETWORK_HOTSPOT);
					LocalBroadcastManager.getInstance(NetworkService.this).sendBroadcast(intent);
					break;
				}
			}
			return false;
		});

	}

	private void initNetworkRunnable() {
		this.networkRunnable = new NetworkRunnable(
				CanvasUtility.getHotspotName(getApplicationContext()), wifiManager, hotspotHandler);
	}

	private boolean isPrevConnectedWifi = false;

	private void sendBroadcast() {
		Intent intent = new Intent(ACTION_CANVAS_STATE);
		intent.putExtra("command", CANVAS_COMMAND_NETWORK_CHANGED);
		LocalBroadcastManager.getInstance(NetworkService.this).sendBroadcast(intent);
	}

	private void initNetworkState() {
		NetworkRequest.Builder builder = new NetworkRequest.Builder();
		networkCallback = new ConnectivityManager.NetworkCallback() {

			@Override
			public void onAvailable(Network network) {
				if (isAvoidFlag)
					return;

				if (isPrevConnectedWifi)
					return;

				isAvoidFlag = true;

				// 네트워크 연결됨
				Log.w(TAG, "NETWORK AVAILABLE:" + network.toString());

				hotspotHandler.postDelayed(() -> {
					isAvoidFlag = false;
					isPrevConnectedWifi = true;

					sendBroadcast();
				}, 100);
			}

			@Override
			public void onLost(Network network) {
				if (isAvoidFlag)
					return;

				if (isPrevConnectedWifi)
					return;

				isAvoidFlag = true;

				// 네트워크 끊어짐
				Log.e(TAG, "NETWORK LOST:" + network.toString());

				hotspotHandler.postDelayed(() -> {
					isAvoidFlag = false;
					isPrevConnectedWifi = false;

					commandHandler.sendEmptyMessage(MSG_COMMAND_HOTSPOT_TURN_ON);
					sendBroadcast();
				}, 100);
			}
		};

		if (cm != null) {
			cm.registerNetworkCallback(builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build(), networkCallback);
		}
	}


	private void startServer() {
		try {
			httpServer = new NetworkHttpServer(SERVER_PORT);
			httpServer.start();

			Log.w(TAG, "HTTP Port is " + httpServer.getListeningPort());
			Log.w(TAG, "HTTP Address is " + Objects.requireNonNull(CanvasUtility.getNetworkAddress()).getHostAddress());
		} catch (IOException e) {
			Log.e(TAG, "HttpServer Start Error", e);
		}
	}

	private void stopServer() {
		if (httpServer != null) {
			httpServer.stop();
		}
	}


	private class NetworkHttpServer extends NanoHttp {

		NetworkHttpServer(int port) {
			super(port);
		}

		@Override
		public Response serve(IHTTPSession session) {
			Log.d(TAG, "getUri=" + session.getUri());

			Map<String, String> files = new HashMap<>();
			Method method = session.getMethod();
			if (Method.PUT.equals(method) || Method.POST.equals(method)) {
				try {
					session.parseBody(files);
				} catch (IOException ioe) {
					return new Response(Response.Status.INTERNAL_ERROR, MIME_PLAINTEXT, "SERVER INTERNAL ERROR: " + ioe.getMessage());
				} catch (ResponseException re) {
					return new Response(re.getStatus(), MIME_PLAINTEXT, re.getMessage());
				}
			}

			switch (session.getUri()) {
				case "/":
					return new NanoHttp.Response("Server Running");

				case "/start": {
					Intent intent = new Intent("SLIDE_SHOW");
					intent.putExtra("command", "start");
					LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
					return new NanoHttp.Response("start OK");
				}

				case "/stop": {
					Intent intent = new Intent("SLIDE_SHOW");
					intent.putExtra("command", "stop");
					LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
					return new NanoHttp.Response("stop OK");
				}

				case "/curation": {
					String postBody = "";
					try {
						postBody = URLDecoder.decode(session.getQueryParameterString(), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						Log.e(TAG, "Decoding Error", e);
					}

					Log.e(TAG, "postBody =>" + postBody);

					Map<String, String> map = getQueryMap(postBody);
					String curationId = map.get("curationId");


					Intent intent = new Intent("SHOW_CURATION");
					intent.putExtra("curationId", curationId);
					LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

					return new NanoHttp.Response("stop OK");
				}

				case "/connect": {
					String postBody = "";
					try {
						postBody = URLDecoder.decode(session.getQueryParameterString(), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						Log.e(TAG, "Decoding Error", e);
					}

					Log.e(TAG, "postBody =>" + postBody);

					Map<String, String> map = getQueryMap(postBody);
					if (TextUtils.isEmpty(map.get("ssid")))
						return new NanoHttp.Response("FAIL");

					// TODO
					connectOnline(map);

					return new NanoHttp.Response("OK");
				}

				default:
					return new NanoHttp.Response("undefined command");
			}
		}

		private Map<String, String> getQueryMap(String query) {
			String[] params = query.split("&");
			Map<String, String> map = new HashMap<>();
			for (String param : params) {
				String name = param.split("=")[0];
				String value = param.split("=")[1];
				map.put(name, value);
			}
			return map;
		}
	}


	public void connectOnline(Map<String, String> map) {
		String ssid = map.get("ssid");
		String password = map.get("password");
		String passwordType = map.get("passwordType");
		String auth = map.get("auth");

		Log.w(TAG, "/connect, <ssid=" + ssid + ", pass=" + password + ", type=" + passwordType + ">");
		Log.w(TAG, "auth=" + auth);
		CanvasUtility.getAuth(getApplicationContext(), auth);

		if (hotspotHandler != null) {
			hotspotHandler.postDelayed(() -> {
				isWifiConnectState = true;

				int count;
				try {
					HotspotControl control = HotspotControl.getApControl(wifiManager);
					if (control != null && control.isWifiApEnabled()) {
						control.setWifiApEnabled(control.getWifiApConfiguration(), false);

						count = 0;
						while (control.isWifiApEnabled()) {
							if (count >= 20) {
								// TODO ERROR
								if (hotspotHandler != null) {
									hotspotTurnOn = new Thread(networkRunnable.HotspotTurnOn);
									hotspotTurnOn.start();
								}
								return;
							}
							Log.d(TAG, "Still waiting for AP to disable...");
							Thread.sleep(500L);
							count++;
						}
					}

					count = 0;
					wifiManager.setWifiEnabled(true);
					Log.i(TAG, "Wi-fi enabled");

					while (!wifiManager.isWifiEnabled()) {
						if (count >= 20) {
							// TODO ERROR
							if (hotspotHandler != null) {
								hotspotTurnOn = new Thread(networkRunnable.HotspotTurnOn);
								hotspotTurnOn.start();
							}
							return;
						}
						Log.i(TAG, "Still waiting for wi-fi to enabling...");
						Thread.sleep(500L);
						count++;
					}


					WifiConfigManager.configure(wifiManager, ssid, password, passwordType);
					stopServer();

				} catch (Exception e) {
					Log.e(TAG, "WIFIConfig Error", e);

					if (hotspotHandler != null) {
						hotspotTurnOn = new Thread(networkRunnable.HotspotTurnOn);
						hotspotTurnOn.start();
					}
				}

			}, 2000);
		}
	}

}
