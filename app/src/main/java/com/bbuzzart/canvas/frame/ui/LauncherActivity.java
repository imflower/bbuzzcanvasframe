package com.bbuzzart.canvas.frame.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.bbuzzart.canvas.frame.R;

public class LauncherActivity extends AppCompatActivity {

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
		super.onCreate(savedInstanceState, persistentState);

		setContentView(R.layout.activity_launcher);

		/*resetPreferredLauncherAndOpenChooser(getApplicationContext());

		new Handler().postDelayed(() -> {
			startActivity(new Intent(LauncherActivity.this, MainActivity.class));
		}, 2000);*/


	}

	public void resetPreferredLauncherAndOpenChooser(Context context) {
		PackageManager packageManager = context.getPackageManager();
		ComponentName componentName = new ComponentName(context, LauncherActivity.class);
		packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

		Intent selector = new Intent(Intent.ACTION_MAIN);
		selector.addCategory(Intent.CATEGORY_HOME);
		selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(selector);

		packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
	}

}
