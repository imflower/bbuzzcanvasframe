package com.bbuzzart.canvas.frame.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.bbuzzart.canvas.frame.CanvasConstant;
import com.bbuzzart.canvas.frame.CanvasUtility;
import com.bbuzzart.canvas.frame.support.NanoHttp;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Objects;

public class NetworkServiceWithNSD extends Service implements NetworkConstant, CanvasConstant {

	public static final String TAG = NetworkServiceWithNSD.class.getSimpleName();

	private static final String DNS_TYPE = "_http._tcp.";


	private Handler hotspotHandler;
	private Handler commandHandler;

	private Thread hotspotTurnOn;
	private NetworkRunnable networkRunnable;
	private NetworkHttpServer httpServer;


	private NetworkState networkState;

	private WifiManager wifiManager;
	private NsdManager nsdManager;
	private NsdManager.RegistrationListener nsdListener;


	private ConnectivityManager cm;
	private ConnectivityManager.NetworkCallback networkCallback;
	private boolean isAvoidFlag;


	private void setNetworkState(NetworkState state) {
		this.networkState = state;
		Log.e(TAG, "NetworkState=" + this.networkState);
	}


	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.w(TAG, "onCreate");
		super.onCreate();

		this.wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		this.nsdManager = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);
		this.cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		initHandler();
		initNetworkState();
		initNetworkRunnable();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent == null) {
			stopSelf();
		} else {
			String action = intent.getAction();
			if (TextUtils.isEmpty(action)) {
				action = "";
			} else {
				Log.w(TAG, "Main Intent Action: " + action);
			}

			switch (action) {
				case ACTION_NETWORK_SERVICE_START: {
					parseActionStart();
					break;
				}
			}
		}

		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy() {
		stopServer();

		if (cm != null && networkCallback != null) {
			cm.unregisterNetworkCallback(networkCallback);
			cm = null;
		}

		super.onDestroy();
	}

	@SuppressLint("HardwareIds")
	private void parseActionStart() {
		if (!CanvasUtility.isWifiConnected(getApplicationContext())) {
			Log.w(TAG, "Network not connected");
			setNetworkState(NetworkState.UNKNOWN);

			commandHandler.sendEmptyMessage(MSG_COMMAND_HOTSPOT_TURN_ON);
		} else {
			setNetworkState(NetworkState.WIFI);

			startServer();
		}
	}

	private void initHandler() {
		commandHandler = new Handler(msg -> {
			switch (msg.what) {
				case MSG_COMMAND_HOTSPOT_TURN_ON: {
					Log.w(TAG, "MSG_COMMAND_HOTSPOT_TURN_ON");
					if (hotspotTurnOn == null || !hotspotTurnOn.isAlive()) {
						hotspotTurnOn = new Thread(networkRunnable.HotspotTurnOn);
						hotspotTurnOn.start();
					} else {
						Log.e(TAG, "MSG_COMMAND_HOTSPOT_TURN_ON running");
					}
					break;
				}
			}
			return false;
		});

		hotspotHandler = new Handler(msg -> {
			switch (msg.what) {
				case MSG_HOTSPOT_TURN_ON_FAIL: {
					Log.w(TAG, "MSG_HOTSPOT_TURN_ON_FAIL");

					if (hotspotTurnOn != null) {
						hotspotTurnOn.interrupt();
						hotspotTurnOn = null;
					}

					stopServer();
					break;
				}
				case MSG_HOTSPOT_TURN_ON_SUCCESS: {
					Log.w(TAG, "MSG_HOTSPOT_TURN_ON_SUCCESS");

					hotspotTurnOn = null;

					setNetworkState(NetworkState.HOTSPOT);

					startServer();
					break;
				}
			}
			return false;
		});

	}

	private void initNetworkRunnable() {
		this.networkRunnable = new NetworkRunnable(
				CanvasUtility.getHotspotName(getApplicationContext()),
				wifiManager, hotspotHandler);
	}

	private void initNetworkState() {
		NetworkRequest.Builder builder = new NetworkRequest.Builder();
		networkCallback = new ConnectivityManager.NetworkCallback() {
			@Override
			public void onAvailable(Network network) {
				if (isAvoidFlag) {
					return;
				}

				isAvoidFlag = true;
				// 네트워크 연결됨
				Log.w(TAG, "NETWORK AVAILABLE:" + network.toString());

				hotspotHandler.postDelayed(() -> {
					isAvoidFlag = false;
				}, 50);
			}

			@Override
			public void onLost(Network network) {
				if (isAvoidFlag) {
					return;
				}

				isAvoidFlag = true;
				// 네트워크 끊어짐
				Log.e(TAG, "NETWORK LOST:" + network.toString());

				hotspotHandler.postDelayed(() -> {
					isAvoidFlag = false;
					commandHandler.sendEmptyMessage(MSG_COMMAND_HOTSPOT_TURN_ON);
				}, 50);
			}
		};

		if (cm != null) {
			cm.registerNetworkCallback(builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build(), networkCallback);
		}
	}


	private void startServer() {
		try {
			ServerSocket mServerSocket;
			int port = 9999;
			/*mServerSocket = new ServerSocket(port);
			port = mServerSocket.getLocalPort();
			mServerSocket.close();*/

			Log.w(TAG, "HTTP Port is " + Integer.toString(port));
			Log.w(TAG, "HTTP Address is " + Objects.requireNonNull(CanvasUtility.getNetworkAddress()).getHostAddress());


			httpServer = new NetworkHttpServer(port);
			httpServer.start();


			Log.w(TAG, "HTTP Fixed Port is " + httpServer.getListeningPort());

			boolean isConnected = CanvasUtility.isWifiConnected(getApplicationContext());

			NsdServiceInfo serviceInfo = new NsdServiceInfo();
			serviceInfo.setServiceType(DNS_TYPE);
			serviceInfo.setServiceName(CanvasUtility.getHotspotName(getApplicationContext()));
			serviceInfo.setHost(CanvasUtility.getNetworkAddress());
			serviceInfo.setPort(port);
			serviceInfo.setAttribute("isConnected", isConnected ? "yes" : "no");


			nsdListener = new NsdManager.RegistrationListener() {
				@Override
				public void onServiceRegistered(NsdServiceInfo NsdServiceInfo) {
					String mServiceName = NsdServiceInfo.getServiceName();
					Log.w(TAG, "NDS Registered service. name used: " + mServiceName);

					// TODO
					// sendLocalBroadcast(TYPE_HOTSPOT);
				}

				@Override
				public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
					Log.e(TAG, "NDS Failed to register service");
				}

				@Override
				public void onServiceUnregistered(NsdServiceInfo arg0) {
					Log.w(TAG, "NDS Unregistered service");
				}

				@Override
				public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
					Log.e(TAG, "NDS Service unregistration failed");
				}
			};

			if (nsdManager != null)
				nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, nsdListener);

		} catch (IOException e) {
			Log.e(TAG, "HttpServer Start Error", e);
		}
	}

	private void stopServer() {
		if (httpServer != null) {
			httpServer.stop();
		}

		if (nsdManager != null && nsdListener != null)
			nsdManager.unregisterService(nsdListener);

		nsdListener = null;
	}



	private class NetworkHttpServer extends NanoHttp {

		NetworkHttpServer(int port) {
			super(port);
		}

		@Override
		public Response serve(IHTTPSession session) {
			Log.d(TAG, "getUri=" + session.getUri());

			return new NanoHttp.Response("undefined command");
		}
	}
}
