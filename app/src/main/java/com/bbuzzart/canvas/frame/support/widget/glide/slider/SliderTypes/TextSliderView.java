package com.bbuzzart.canvas.frame.support.widget.glide.slider.SliderTypes;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

import com.bbuzzart.canvas.frame.R;
import com.bbuzzart.canvas.frame.support.widget.flaviofaria.kenburnsview.KenBurnsView;
import com.bbuzzart.canvas.frame.support.widget.flaviofaria.kenburnsview.RandomTransitionGenerator;

/**
 * This is a slider with a description TextView.
 */
public class TextSliderView extends BaseSliderView {

	public FrameLayout background;
	public AppCompatTextView description;
	public AppCompatTextView artistName;

	public AppCompatImageView glide_slider_image;
	public KenBurnsView kenBurnsView;

	public void setBackground(int color) {
		background.setBackgroundResource(color);
	}

	public String fitType;

	public TextSliderView(Context context, String fitType) {
		super(context);
		this.fitType = fitType;
	}

	@Override
	public View getView() {
		View v = LayoutInflater.from(getContext()).inflate(R.layout.render_type_text, null);

		background = v.findViewById(R.id.glide_slider_background);
		kenBurnsView = v.findViewById(R.id.kenBurnsView);
		glide_slider_image = v.findViewById(R.id.glide_slider_image);
		description = v.findViewById(R.id.description);
		description.setText(getDescription());
		artistName = v.findViewById(R.id.artistName);
		artistName.setText(getArtistName());

		AccelerateDecelerateInterpolator ACCELERATE_DECELERATE = new AccelerateDecelerateInterpolator();
		RandomTransitionGenerator generator = new RandomTransitionGenerator(30000, ACCELERATE_DECELERATE);
		kenBurnsView.setTransitionGenerator(generator);

		switch (fitType) {
			case "cover":
			case "contain":
				glide_slider_image.setVisibility(View.VISIBLE);
				kenBurnsView.setVisibility(View.GONE);

				bindEventAndShow(v, glide_slider_image);
				break;
			case "random":
				glide_slider_image.setVisibility(View.GONE);
				kenBurnsView.setVisibility(View.VISIBLE);

				bindEventAndShow(v, kenBurnsView);
				break;
			case "scroll":
				glide_slider_image.setVisibility(View.GONE);
				kenBurnsView.setVisibility(View.VISIBLE);
				
				bindEventAndShow(v, kenBurnsView);
				break;
		}

		return v;
	}
}
