package com.bbuzzart.canvas.frame.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

public class CanvasMonitorService extends Service {

	public static final String TAG = CanvasMonitorService.class.getSimpleName();


	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public static Thread mThread;

	private ComponentName recentComponentName;
	private ActivityManager mActivityManager;

	private boolean serviceRunning = false;

	@Override
	public void onCreate() {
		super.onCreate();

		mActivityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		serviceRunning = true;
	}

	@Override
	public void onDestroy() {
		serviceRunning = false;
		super.onDestroy();
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (mThread == null) {
			mThread = new Thread(() -> {
				while (serviceRunning) {
					List<ActivityManager.RecentTaskInfo> info =
							mActivityManager.getRecentTasks(1, Intent.FLAG_ACTIVITY_NEW_TASK);

					if (info != null) {
						ActivityManager.RecentTaskInfo recent = info.get(0);
						Intent mIntent = recent.baseIntent;
						ComponentName name = mIntent.getComponent();

						if (name != null && name.equals(recentComponentName)) {
							Log.d(TAG, "== pre App, recent App is same App");
						} else {
							recentComponentName = name;
							Log.d(TAG, "== Application is captured: " + name);
						}
					}

					SystemClock.sleep(2000);
				}
			});

			mThread.start();
		} else if (!mThread.isAlive()) {
			mThread.start();
		}

		return START_STICKY;
	}
}
