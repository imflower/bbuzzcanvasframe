/*
 * Created by imflower on 14/02/2018.
 */

package com.bbuzzart.canvas.frame.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bbuzzart.canvas.frame.ui.MainActivity;

public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Log.e("BOOT_SIGNAGE", "RECEIVED_BOOT_COMPLETED");

			Intent i = new Intent(context, MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}
	}

}
