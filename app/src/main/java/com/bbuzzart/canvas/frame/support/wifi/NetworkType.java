package com.bbuzzart.canvas.frame.support.wifi;

public enum NetworkType {

	WEP, WPA, NO_PASSWORD;

	public static NetworkType forIntentValue(String networkTypeString) {
		if (networkTypeString == null) {
			return NO_PASSWORD;
		}

		if (networkTypeString.contains("WPA2")) {
			return WPA;
		}
		else if (networkTypeString.contains("WPA")) {
			return WPA;
		}
		else if (networkTypeString.contains("WEP")) {
			return WEP;
		} else if (networkTypeString.contains("nopass")) {
			return NO_PASSWORD;
		}

		throw new IllegalArgumentException(networkTypeString);
	}
}
