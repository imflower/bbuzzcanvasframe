package com.bbuzzart.canvas.frame.service;

import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import com.bbuzzart.canvas.frame.support.HotspotControl;

public class NetworkRunnable implements NetworkConstant {

	private static final String TAG = NetworkRunnable.class.getSimpleName();

	private String hotspotName;
	private WifiManager wifiManager;
	private Handler handler;

	NetworkRunnable(String hotspotName, WifiManager wifiManager, Handler handler) {
		this.hotspotName = hotspotName;
		this.wifiManager = wifiManager;
		this.handler = handler;
	}


	public Runnable WifiTurnOn = new Runnable() {
		@Override
		public void run() {
			int count;
			try {
				HotspotControl control = HotspotControl.getApControl(wifiManager);
				if (control != null && control.isWifiApEnabled()) {
					control.setWifiApEnabled(control.getWifiApConfiguration(), false);

					count = 0;
					while (control.isWifiApEnabled()) {
						if (count >= 20) {
							// TODO ERROR
							handler.sendEmptyMessage(MSG_WIFI_TURN_ON_FAIL);
							return;
						}
						Log.d(TAG, "Still waiting for AP to disable...");
						Thread.sleep(1000L);
						count++;
					}
				}

				count = 0;
				wifiManager.setWifiEnabled(true);
				Log.i(TAG, "Wi-fi enabled");

				while (!wifiManager.isWifiEnabled()) {
					if (count >= 20) {
						// TODO ERROR
						handler.sendEmptyMessage(MSG_WIFI_TURN_ON_FAIL);
						return;
					}
					Log.i(TAG, "Still waiting for wi-fi to enabling...");
					Thread.sleep(1000L);
					count++;
				}

				handler.sendEmptyMessage(MSG_WIFI_TURN_ON_SUCCESS);

			} catch (Exception e) {
				Log.e(TAG, "WIFIConfig Error", e);

				handler.sendEmptyMessage(MSG_WIFI_TURN_ON_FAIL);
			}
		}
	};


	public Runnable HotspotTurnOn = new Runnable() {
		@Override
		public void run() {
			int count;
			try {
				count = 0;
				wifiManager.setWifiEnabled(false);
				Log.i(TAG, "Wi-fi disable");

				while (wifiManager.isWifiEnabled()) {
					if (count >= 20) {
						// TODO ERROR
						handler.sendEmptyMessage(MSG_HOTSPOT_TURN_ON_FAIL);
						return;
					}
					Log.i(TAG, "Still waiting for wi-fi to disabling...");
					Thread.sleep(1000L);
					count++;
				}

				HotspotControl control = HotspotControl.getApControl(wifiManager);
				if (control != null && !control.isWifiApEnabled()) {
					control.setHotspotName(control.getWifiApConfiguration(), hotspotName);
					control.setWifiApEnabled(control.getWifiApConfiguration(), true);

					count = 0;
					while (!control.isWifiApEnabled()) {
						if (count >= 20) {
							// TODO
							Log.e(TAG, "Error Took too long to enabling AP, quitting");
							return;
						}
						Log.d(TAG, "Still waiting for AP to enable...");
						Thread.sleep(1000L);
						count++;
					}
				}

				handler.sendEmptyMessage(MSG_HOTSPOT_TURN_ON_SUCCESS);
			} catch (Exception e) {
				Log.e(TAG, "WIFIConfig Error", e);

				handler.sendEmptyMessage(MSG_HOTSPOT_TURN_ON_FAIL);
			}
		}
	};





	/*
	Runnable WifiTurnOn = new Runnable() {
		@Override
		public void run() {
			int count;
			try {
				HotspotControl control = HotspotControl.getApControl(wifiManager);
				if (control != null && control.isWifiApEnabled()) {
					control.setWifiApEnabled(control.getWifiApConfiguration(), false);

					count = 0;
					while (control.isWifiApEnabled()) {
						if (count >= 10) {
							// TODO
							Log.e(TAG, "Error Took too long to enable AP, quitting");
							return;
						}
						Log.d(TAG, "Still waiting for AP to disable...");
						Thread.sleep(500L);
						count++;
					}
				}

				handler.sendEmptyMessage(MSG_HOTSPOT_TURN_OFF_SUCCESS);
			} catch (Exception e) {
				Log.e(TAG, "WIFIConfig Error", e);

				handler.sendEmptyMessage(MSG_HOTSPOT_TURN_OFF_FAIL);
			}
		}
	};

	Runnable WifiSetup = new Runnable() {
		@Override
		public void run() {
			int count;
			try {
				HotspotControl control = HotspotControl.getApControl(wifiManager);
				if (control != null) {
					control.setWifiApEnabled(control.getWifiApConfiguration(), false);

					count = 0;
					while (control.isWifiApEnabled()) {
						if (count >= 10) {
							// TODO
							Log.e(TAG, "Error Took too long to enable AP, quitting");
							return;
						}
						Log.d(TAG, "Still waiting for AP to disable...");
						Thread.sleep(500L);
						count++;
					}
				}

				wifiManager.setWifiEnabled(false);
				Log.i(TAG, "Wi-fi disabled");

				count = 0;
				while (wifiManager.isWifiEnabled()) {
					if (count >= 10) {
						// TODO
						Log.e(TAG, "Error Took too long to enable wi-fi, quitting");
						return;
					}
					Log.d(TAG, "Still waiting for wi-fi to enable...");
					Thread.sleep(500L);
					count++;
				}


				count = 0;
				wifiManager.setWifiEnabled(true);
				Log.i(TAG, "Wi-fi enabled");

				while (!wifiManager.isWifiEnabled()) {
					if (count >= 10) {
						// TODO
						Log.e(TAG, "Took too long to enable wi-fi, quitting");
						return;
					}
					Log.i(TAG, "Still waiting for wi-fi to enable...");
					Thread.sleep(500L);
					count++;
				}

				isApScanning = true;
				wifiManager.startScan();
			} catch (Exception e) {
				Log.e(TAG, "WIFIConfig Error", e);
			}
		}
	};

	Runnable HotspotSetup = new Runnable() {
		@Override
		public void run() {
			int count = 0;
			try {
				count = 0;
				if (wifiManager.isWifiEnabled()) {
					wifiManager.setWifiEnabled(false);
				}
				while (wifiManager.isWifiEnabled()) {
					if (count >= 10) {
						Log.e(TAG, "Error Took too long to enable wi-fi, quitting");
						return;
					}
					Log.d(TAG, "Still waiting for wi-fi to enable...");
					Thread.sleep(500L);
					count++;
				}

				HotspotControl control = HotspotControl.getApControl(wifiManager);
				if (control != null) {
					control.setHotspotName(control.getWifiApConfiguration(), hotspotName);
					control.setWifiApEnabled(control.getWifiApConfiguration(), true);

					count = 0;
					while (!control.isWifiApEnabled()) {
						if (count >= 10) {
							// TODO
							Log.e(TAG, "@ Error Took too long to enable AP, quitting");
							return;
						}
						Log.d(TAG, "@ Still waiting for AP to enable...");
						Thread.sleep(500L);
						count++;
					}
				}

				startServer();
			} catch (Exception e) {
				Log.e(TAG, "HotspotSetup", e);
			}
		}
	};
	*/
}
