package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Parcel(Parcel.Serialization.BEAN)
@NoArgsConstructor
public class CanvasRegistModel {

	@SerializedName("canvasDisplayId")
	@Expose
	public String canvasDisplayId;

	@SerializedName("canvasName")
	@Expose
	public String canvasName;

	@SerializedName("canvasUid")
	@Expose
	public String canvasUid;

	@SerializedName("wifiName")
	@Expose
	public String wifiName;

	public CanvasRegistModel(String canvasDisplayId, String canvasName, String canvasUid, String wifiName) {
		this.canvasDisplayId = canvasDisplayId;
		this.canvasName = canvasName;
		this.canvasUid = canvasUid;
		this.wifiName = wifiName;
	}
}
