package com.bbuzzart.canvas.network.base;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public abstract class ServerResponse {
	private Boolean status;
	private String code;
	private String info;
	private String message;
}
