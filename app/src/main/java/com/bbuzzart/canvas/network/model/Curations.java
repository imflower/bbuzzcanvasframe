/*
 * Created by imDangerous on 13/02/2018.
 */

package com.bbuzzart.canvas.network.model;

import com.bbuzzart.canvas.network.base.ServerResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Parcel(value = Parcel.Serialization.BEAN, analyze = Curations.class)
@Getter
@Setter
@ToString
public class Curations extends ServerResponse {

	@SerializedName("data")
	@Expose
	public Data data = new Data();

	@Getter
	@Setter
	@ToString
	@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
	public static class Data {

		@SerializedName("curations")
		@Expose
		public List<Curation> curations = new ArrayList<>();


		@SerializedName("pagination")
		@Expose
		public Pagination pagination = new Pagination();


		@Getter
		@Setter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Curation.class)
		public static class Curation {

			@SerializedName("curationId")
			@Expose
			public Integer curationId;
			@SerializedName("curationTitle")
			@Expose
			public String curationTitle;
			@SerializedName("curationComment")
			@Expose
			public String curationComment;
			@SerializedName("curatorId")
			@Expose
			public Integer curatorId;
			@SerializedName("curatorName")
			@Expose
			public String curatorName;
			@SerializedName("curatorProfileImgUrl")
			@Expose
			public String curatorProfileImgUrl;
			@SerializedName("curatorProfileImgsUrl")
			@Expose
			public String curatorProfileImgsUrl;
			@SerializedName("curatorProfileImgmUrl")
			@Expose
			public String curatorProfileImgmUrl;
			@SerializedName("pick")
			@Expose
			public Pick pick;
		}

	}

}
