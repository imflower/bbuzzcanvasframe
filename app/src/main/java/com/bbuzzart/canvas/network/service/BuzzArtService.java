/*
 * Created by imDangerous on 13/02/2018.
 */

package com.bbuzzart.canvas.network.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bbuzzart.canvas.network.model.BuzzPicks;
import com.bbuzzart.canvas.network.model.CanvasViewCount;
import com.bbuzzart.canvas.network.model.Canvases;
import com.bbuzzart.canvas.network.model.CanvasRegistModel;
import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.network.model.CuratorPicks;
import com.bbuzzart.canvas.network.model.Initialize;
import com.bbuzzart.canvas.network.model.SignAccount;
import com.bbuzzart.canvas.network.model.Success;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BuzzArtService {

	@GET("initialize")
	Observable<Initialize> fetchInitialize();

	@GET("bz-picks")
	Observable<BuzzPicks> fetchBuzzPicks(
			@Header("Authorization") @Nullable String authorization,
			@Query("page") @Nullable Integer page,
			@Query("size") @Nullable Integer size,
			@Query("random") @NonNull Boolean random
	);

	@GET("curations")
	Observable<Curations> fetchCurations(
			@Header("Authorization") @Nullable String authorization,
			@Query("all") @Nullable Boolean all,
			@Query("type") @Nullable String type,
			@Query("page") @Nullable Integer page,
			@Query("size") @Nullable Integer size
	);

	@GET("curations/{curationId}/picks")
	Observable<CuratorPicks> fetchCuratorPicks(
			@Path("curationId") @NonNull Integer curationId,
			@Header("Authorization") @Nullable String authorization,
			@Query("all") @Nullable Boolean all,
			@Query("page") @Nullable Integer page,
			@Query("size") @Nullable Integer size
	);

	@GET("curations/{curationId}/picks/{pickId}")
	Observable<CuratorPicks> fetchCuratorPickSingle(
			@Path("curationId") @NonNull Integer curationId,
			@Path("pickId") @NonNull Integer pickId,
			@Header("Authorization") @Nullable String authorization
	);

	@POST("curations/{curationId}/picks/{pickId}/view")
	Observable<Success> doPostPickViewCount(
			@Path("curationId") @NonNull Integer curationId,
			@Path("pickId") @NonNull Integer pickId,
			@Header("Authorization") @Nullable String authorization,
			@Body CanvasViewCount canvasViewForm
	);


	@POST("account/login-auth")
	Observable<SignAccount> doPostAuthSign(
			@Header("Authorization") @NonNull String authorization
	);

	@POST("account/login-google")
	Observable<SignAccount> doPostGoogleSign(
			@Query("gEmail") @NonNull String email,
			@Query("gUsrName") @NonNull String usrName,
			@Query("gClientId") @NonNull String clientId,
			@Query("gAccessToken") @NonNull String accessToken
	);

	@POST("account/login-facebook")
	Observable<SignAccount> doPostFacebookSign(
			@Query("fbEmail") @NonNull String email,
			@Query("fbUsrName") @NonNull String usrName,
			@Query("fbClientId") @NonNull String clientId,
			@Query("fbAccessToken") @NonNull String accessToken
	);

	@GET("canvases/init")
	Observable<Canvases> doGetCanvasInit(
			@Header("Authorization") @Nullable String authorization,
			@Query("canvasUid") @NonNull String canvasUid
	);


	@POST("canvases")
	Observable<Canvases> doPostCanvasRegist(
			@Header("Authorization") @Nullable String authorization,
			@Body CanvasRegistModel canvasForm
	);
}
