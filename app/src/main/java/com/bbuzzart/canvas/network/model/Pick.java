package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by imDangerous on 28/03/2018.
 */

@Getter
@Setter
@ToString
@Parcel(value = Parcel.Serialization.BEAN, analyze = Pick.class)
public class Pick {

	@SerializedName("pickId")
	@Expose
	public Integer pickId;
	@SerializedName("pickComment")
	@Expose
	public String pickComment;
	@SerializedName("workId")
	@Expose
	public Integer workId;
	@SerializedName("workTitle")
	@Expose
	public String workTitle;
	@SerializedName("workNote")
	@Expose
	public String workNote;
	@SerializedName("workYear")
	@Expose
	public Integer workYear;
	@SerializedName("workCategory")
	@Expose
	public String workCategory;
	@SerializedName("workWidth")
	@Expose
	public Float workWidth;
	@SerializedName("workHeight")
	@Expose
	public Float workHeight;
	@SerializedName("workDepth")
	@Expose
	public Float workDepth;
	@SerializedName("workSizeUnit")
	@Expose
	public String workSizeUnit;
	@SerializedName("workMaterials")
	@Expose
	public String workMaterials;
	@SerializedName("workImgUrl")
	@Expose
	public String workImgUrl;
	@SerializedName("workImgxxsUrl")
	@Expose
	public String workImgxxsUrl;
	@SerializedName("workImgxsUrl")
	@Expose
	public String workImgxsUrl;
	@SerializedName("workImgsUrl")
	@Expose
	public String workImgsUrl;
	@SerializedName("workImgmUrl")
	@Expose
	public String workImgmUrl;
	@SerializedName("workImglUrl")
	@Expose
	public String workImglUrl;
	@SerializedName("workImgWidth")
	@Expose
	public Integer workImgWidth;
	@SerializedName("workImgHeight")
	@Expose
	public Integer workImgHeight;
	@SerializedName("workPrice")
	@Expose
	public Integer workPrice;
	@SerializedName("workOriginalPrice")
	@Expose
	public Integer workOriginalPrice;
	@SerializedName("workSaleStatus")
	@Expose
	public Integer workSaleStatus;
	@SerializedName("workDiscountPrice")
	@Expose
	public Float workDiscountPrice;
	@SerializedName("workDiscountRatio")
	@Expose
	public Float workDiscountRatio;
	@SerializedName("artistId")
	@Expose
	public Integer artistId;
	@SerializedName("artistName")
	@Expose
	public String artistName;
	@SerializedName("artistProfileImgUrl")
	@Expose
	public String artistProfileImgUrl;
	@SerializedName("artistProfileImgsUrl")
	@Expose
	public String artistProfileImgsUrl;
	@SerializedName("artistProfileImgmUrl")
	@Expose
	public String artistProfileImgmUrl;
	@SerializedName("curationId")
	@Expose
	public Integer curationId;

}