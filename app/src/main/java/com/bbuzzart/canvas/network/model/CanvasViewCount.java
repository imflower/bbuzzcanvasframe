package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Parcel(Parcel.Serialization.BEAN)
@NoArgsConstructor
public class CanvasViewCount {

	@SerializedName("canvasId")
	@Expose
	public Integer canvasId;

	public CanvasViewCount(int canvasId) {
		this.canvasId = canvasId;
	}
}
