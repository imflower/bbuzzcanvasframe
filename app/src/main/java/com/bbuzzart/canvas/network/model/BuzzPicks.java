/*
 * Created by imDangerous on 13/02/2018.
 */

package com.bbuzzart.canvas.network.model;

import com.bbuzzart.canvas.network.base.ServerResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Parcel(value = Parcel.Serialization.BEAN, analyze = BuzzPicks.class)
@Getter
@Setter
@ToString
public class BuzzPicks extends ServerResponse {

	@SerializedName("data")
	@Expose
	public Data data = new Data();

	@Getter
	@Setter
	@ToString
	@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
	public static class Data {

		@SerializedName("buzzPicks")
		@Expose
		public List<BuzzPick> buzzPicks = new ArrayList<>();


		@SerializedName("pagination")
		@Expose
		public Pagination pagination = new Pagination();


		@Getter
		@Setter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = BuzzPick.class)
		public static class BuzzPick {

			@SerializedName("bzpId")
			@Expose
			public Integer bzpId;
			@SerializedName("wokId")
			@Expose
			public Integer wokId;
			@SerializedName("comment")
			@Expose
			public String comment;
			@SerializedName("createUser")
			@Expose
			public Integer createUser;
			@SerializedName("updateDate")
			@Expose
			public String updateDate;
			@SerializedName("createDate")
			@Expose
			public String createDate;
			@SerializedName("work")
			@Expose
			public Work work;
			@SerializedName("user")
			@Expose
			public User user;

		}

	}

}
