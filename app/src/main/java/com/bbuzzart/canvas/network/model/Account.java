package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Parcel(Parcel.Serialization.BEAN)
public class Account {

	@SerializedName("usrId")
	@Expose
	public Integer usrId;
	@SerializedName("name")
	@Expose
	public String name;
	@SerializedName("email")
	@Expose
	public String email;
	@SerializedName("profileImgUrl")
	@Expose
	public String profileImgUrl;
	@SerializedName("profileImgsUrl")
	@Expose
	public String profileImgsUrl;
	@SerializedName("profileImgmUrl")
	@Expose
	public String profileImgmUrl;
	@SerializedName("thumbnailImgUrl")
	@Expose
	public String thumbnailImgUrl;
	@SerializedName("description")
	@Expose
	public String description;
	@SerializedName("education")
	@Expose
	public String education;
	@SerializedName("auth")
	@Expose
	public String auth;
	@SerializedName("rcmdUsrId")
	@Expose
	public String rcmdUsrId;
	@SerializedName("rcmdCode")
	@Expose
	public String rcmdCode;
	@SerializedName("urls")
	@Expose
	public String urls;
	@SerializedName("coverImgUrl")
	@Expose
	public String coverImgUrl;
	@SerializedName("notificationCount")
	@Expose
	public Integer notificationCount;
	@SerializedName("followingCount")
	@Expose
	public Integer followingCount;
	@SerializedName("followerCount")
	@Expose
	public Integer followerCount;
	@SerializedName("ctcId")
	@Expose
	public Integer ctcId;
	@SerializedName("artEmail")
	@Expose
	public String artEmail;
	@SerializedName("paypalEmail")
	@Expose
	public String paypalEmail;
	@SerializedName("areaCode")
	@Expose
	public String areaCode;
	@SerializedName("tel")
	@Expose
	public String tel;
	@SerializedName("addr")
	@Expose
	public String addr;
	@SerializedName("addrDetail")
	@Expose
	public String addrDetail;
	@SerializedName("country")
	@Expose
	public String country;
	@SerializedName("city")
	@Expose
	public String city;
	@SerializedName("region")
	@Expose
	public String region;
	@SerializedName("legalName")
	@Expose
	public String legalName;
	@SerializedName("zipCode")
	@Expose
	public String zipCode;
	@SerializedName("accName")
	@Expose
	public String accName;
	@SerializedName("accNumber")
	@Expose
	public String accNumber;
	@SerializedName("bankName")
	@Expose
	public String bankName;
	@SerializedName("bankRoutingNumber")
	@Expose
	public String bankRoutingNumber;
	@SerializedName("isIdentityConfirm")
	@Expose
	public Boolean isIdentityConfirm;
	@SerializedName("isEmailConfirmed")
	@Expose
	public Boolean isEmailConfirmed;
	@SerializedName("isPushNotification")
	@Expose
	public Boolean isPushNotification;
	@SerializedName("hasPassword")
	@Expose
	public Boolean hasPassword;
	@SerializedName("isArtEmailConfirm")
	@Expose
	public Boolean isArtEmailConfirm;
	@SerializedName("isTelConfirm")
	@Expose
	public Boolean isTelConfirm;
	@SerializedName("userAuthority")
	@Expose
	public UserAuthority userAuthority;
	@SerializedName("artistTypes")
	@Expose
	public List<String> artistTypes = new ArrayList<>();


	@Getter
	@ToString
	@Parcel(Parcel.Serialization.BEAN)
	public static class UserAuthority {

		@SerializedName("master")
		@Expose
		public boolean master;
		@SerializedName("admin")
		@Expose
		public boolean admin;
		@SerializedName("curator")
		@Expose
		public boolean curator;
		@SerializedName("artist")
		@Expose
		public boolean artist;
		@SerializedName("user")
		@Expose
		public boolean user;
		@SerializedName("anoymous")
		@Expose
		public boolean anoymous;
		@SerializedName("blacklist")
		@Expose
		public boolean blacklist;

	}
}

