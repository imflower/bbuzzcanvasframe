package com.bbuzzart.canvas.network.model;

import com.bbuzzart.canvas.network.base.ServerResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Parcel(value = Parcel.Serialization.BEAN, analyze = Success.class)
public class Success extends ServerResponse {

	@SerializedName("data")
	@Expose
	public Data data = new Data();

	@Getter
	@Setter
	@ToString
	@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
	public static class Data {

	}

}

