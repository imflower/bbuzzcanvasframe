/*
 * Created by imDangerous on 13/02/2018.
 */

package com.bbuzzart.canvas.network.model;

import com.bbuzzart.canvas.network.base.ServerResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Parcel(value = Parcel.Serialization.BEAN, analyze = CuratorPicks.class)
@Getter
@Setter
@ToString
public class CuratorPicks extends ServerResponse {

	@SerializedName("data")
	@Expose
	public Data data = new Data();

	@Getter
	@Setter
	@ToString
	@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
	public static class Data {

		@SerializedName("picks")
		@Expose
		public List<Pick> picks = new ArrayList<>();


		@SerializedName("pagination")
		@Expose
		public Pagination pagination = new Pagination();

	}

}
