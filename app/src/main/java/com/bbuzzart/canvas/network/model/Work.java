package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Parcel(Parcel.Serialization.BEAN)
public class Work {

	@SerializedName("wokId")
	@Expose
	public int wokId;
	@SerializedName("type")
	@Expose
	public String type;
	@SerializedName("title")
	@Expose
	public String title;
	@SerializedName("note")
	@Expose
	public String note;
	@SerializedName("year")
	@Expose
	public int year;
	@SerializedName("likeCount")
	@Expose
	public Object likeCount;
	@SerializedName("feedbackCount")
	@Expose
	public Object feedbackCount;
	@SerializedName("shareCount")
	@Expose
	public Object shareCount;
	@SerializedName("bookmarkCount")
	@Expose
	public Object bookmarkCount;
	@SerializedName("viewCount")
	@Expose
	public Object viewCount;
	@SerializedName("isLike")
	@Expose
	public boolean isLike;
	@SerializedName("isBookmark")
	@Expose
	public boolean isBookmark;
	@SerializedName("isContainCart")
	@Expose
	public boolean isContainCart;
	@SerializedName("price")
	@Expose
	public Object price;
	@SerializedName("saleStatus")
	@Expose
	public int saleStatus;
	@SerializedName("createUser")
	@Expose
	public int createUser;
	@SerializedName("updateDate")
	@Expose
	public String updateDate;
	@SerializedName("createDate")
	@Expose
	public String createDate;
	@SerializedName("user")
	@Expose
	public User user;
	@SerializedName("tags")
	@Expose
	public Object tags;
	@SerializedName("workFeedbacks")
	@Expose
	public Object workFeedbacks;
	@SerializedName("workImage")
	@Expose
	public WorkImage workImage;
	@SerializedName("workVideo")
	@Expose
	public WorkVideo workVideo;
	@SerializedName("workPackage")
	@Expose
	public Object workPackage;
	@SerializedName("attachments")
	@Expose
	public List<Attachment> attachments = null;



	@Getter
	@ToString
	@Parcel(Parcel.Serialization.BEAN)
	public static class WorkVideo {

		@SerializedName("wkvId")
		@Expose
		public Integer wkvId;
		@SerializedName("wokId")
		@Expose
		public Integer wokId;
		@SerializedName("serviceType")
		@Expose
		public String serviceType;
		@SerializedName("videoId")
		@Expose
		public String videoId;
		@SerializedName("videoUrl")
		@Expose
		public String videoUrl;
		@SerializedName("note")
		@Expose
		public String note;
		@SerializedName("duration")
		@Expose
		public Integer duration;
		@SerializedName("year")
		@Expose
		public String year;
		@SerializedName("updateDate")
		@Expose
		public String updateDate;
		@SerializedName("createDate")
		@Expose
		public String createDate;
	}


	@Getter
	@ToString
	@Parcel(Parcel.Serialization.BEAN)
	public static class WorkImage {

		@SerializedName("wkiId")
		@Expose
		public int wkiId;
		@SerializedName("wokId")
		@Expose
		public int wokId;
		@SerializedName("category")
		@Expose
		public String category;
		@SerializedName("edition")
		@Expose
		public String edition;
		@SerializedName("width")
		@Expose
		public String width;
		@SerializedName("height")
		@Expose
		public String height;
		@SerializedName("depth")
		@Expose
		public String depth;
		@SerializedName("sizeUnit")
		@Expose
		public String sizeUnit;
		@SerializedName("materials")
		@Expose
		public String materials;
		@SerializedName("geoLocation")
		@Expose
		public Object geoLocation;

	}


	@Getter
	@ToString
	@Parcel(Parcel.Serialization.BEAN)
	public static class Attachment {

		@SerializedName("watId")
		@Expose
		public int watId;
		@SerializedName("imgxxsUrl")
		@Expose
		public String imgxxsUrl;
		@SerializedName("imgxsUrl")
		@Expose
		public String imgxsUrl;
		@SerializedName("imgsUrl")
		@Expose
		public String imgsUrl;
		@SerializedName("imgmUrl")
		@Expose
		public String imgmUrl;
		@SerializedName("imglUrl")
		@Expose
		public String imglUrl;
		@SerializedName("imgWidth")
		@Expose
		public int imgWidth;
		@SerializedName("imgHeight")
		@Expose
		public int imgHeight;
		@SerializedName("isMain")
		@Expose
		public boolean isMain;
		@SerializedName("order")
		@Expose
		public int order;

	}
}
